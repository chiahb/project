/**
 * Created by Francis Chia on 8/15/2016.
 */


var mysql = require("mysql");

var pool = mysql.createPool({
    host: "localhost",
    port: 3306,
    user: "root",
    password: "password",
    database: "dow",
    connectionLimit: 4
});

const INSERT_USERS_SQL = "insert into users (email, password, google_id, facebook_id) values (?,?,?,?)";
const INSERT_DOC_SQL = "insert into doctors (smc_no, degree, university, field) values (?,?,?,?)";

const SELECTSQL = "select * from users where email =? OR name_last = ? OR name_first = ? limit 10";
const SELECTIDSQL = "select * from doctors where users_id = ?";

const UPDATESQL = "update doctors SET smc_no=?, degree=?, university=? field=? WHERE users_id=?";

const DELETESQL = "delete from doctors where users_id=?";



var doclist = function () {

};

// function userlist () {
//    
// }
doclist.saveUser = function (values, callback) {
    pool.getConnection(function (err, connection) {
        if (err) {
            return callback(err);
        }
        connection.query(INSERT_USERS_SQL, values, function (err, result) {
            connection.release();
            if (err) {
                return callback(err, null);
            }
            callback(null, result);

        });
    });
};

doclist.saveDoc = function (values, callback) {
    pool.getConnection(function (err, connection) {
        if (err) {
            return callback(err);
        }
        connection.query(INSERT_DOC_SQL, values, function (err, result) {
            connection.release();
            if (err) {
                return callback(err, null);
            }
            callback(null, result);

        });
    });
};


doclist.getAllusers = function (callback) {
    var doclist = this;
    pool.getConnection(function (err, connection) {
        if (err) {
            return callback(err);
        }
        connection.query(SELECTSQL, [], function (err, results) {
            connection.release();
            if (err) {
                return callback(err, null);
            }
            callback(null, results);
        });
    });
};



// doclist.getAllDocs = function (params, callback) {
//     if(! params ) {
//         params = {
//             email: "",
//             name_last: "",
//             name_first:""
//         };
//     }
//     console.log(params)
//     var doclist = this;
//     pool.getConnection(function (err, connection) {
//         if (err) {
//             return callback(err);
//         }
//         connection.query(SELECTSQL, [params.email, params.name_last, params.name_first], function (err, results) {
//             connection.release();
//             console.log(results)
//             if (err) {
//                 return callback(err);
//             }
//             callback(null, results);
//         });
//     });
// };


doclist.getDoc = function (params, callback) {

    var doclist = this;
    pool.getConnection(function (err, connection) {
        if (err) {
            return callback(err);
        }
        
        connection.query(SELECTIDSQL, [params.users_id], function (err, results) {
            connection.release();
            console.log(results)
            if (err) {
                return callback(err, null);
            }
            callback(null, results);
        });
    });
};

doclist.updateDoc = function (params, callback) {

    var doclist = this;
    pool.getConnection(function (err, connection) {
        if (err) {
            return callback(err);
        }
        connection.query(UPDATESQL, [ params.smc_no, params.degree, params.university, params.field, params.id], function (err, results) {
            connection.release();
            console.log(results)
            if (err) {
                return callback(err, null);
            }
            callback(null, results);
        });
    });
};



doclist.deleteDoc = function (callback) {
    var doclist = this;
    pool.getConnection(function (err, connection) {
        if (err) {
            console.error(err);
            return callback(err);
        }
        connection.query(DELETESQL, [doclist.id], function (err, results) {
            connection.release();
            if (err) {
                console.error(err);
                return callback(err, null);
            }
            callback(null, results);
        });
    });
};


module.exports = doclist;
// module.exports = userlist;

