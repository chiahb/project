(function () {
    angular.module("dowApp")
        .service("docService", docService);

    docService.$inject = ["$http", "$q"];

    function docService($http, $q) {
        var service = this;

        service.getNewDoc = function () {
            return {
                username :"",
                password: "" ,
                google_acct: "",
                facebook_acct: "",
                user_type:"d",
                twitter_acct: "",
                smc: "",
                degree:"",
                university:"",
                field:""
            };
        };

        service.saveDoc = function (doc, callback) {
            var defer = $q.defer();
            $http.post("/api/doc/saveDoc", doc).then(function (result) {
                defer.resolve(result);
            }).catch(function (err) {
                defer.reject(err);
            });

            return defer.promise;
        };
        service.saveUser = function (doc, callback) {
            var defer = $q.defer();
            $http.post("/api/doc/saveUser", doc).then(function (result) {
                defer.resolve(result);
            }).catch(function (err) {
                defer.reject(err);
            });

            return defer.promise;
        };

        service.listDoc = function (docid) {
            var defer = $q.defer();
            console.info(docId);
            $http.put("/api/doc/list", {docId: docId}).then(function (result) {
                defer.resolve(result);
            }).catch(function (err) {
                defer.reject(err);
            });

            return defer.promise;
        };

        service.updateDoc = function (doc, callback){
            var defer = $q.defer();
            console.info(docId);
            $http.put("/api/doc/update", {docId: docId}).then(function (result) {
                defer.resolve(result);
            }).catch(function (err) {
                defer.reject(err);
            });

            return defer.promise;
        };

        service.deleteDoc = function (docId, callback){
            var defer = $q.defer();
            console.info("service delete Doc");
            console.info(docId);
            $http.delete("/api/doc/delete?docId=" + docId).then(function (result) {
                defer.resolve(result);
            }).catch(function (err) {
                defer.reject(err);
            });

            return defer.promise;
        };

    }
})();
