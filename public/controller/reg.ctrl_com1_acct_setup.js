(function () {
    angular.module("dowApp")
        .controller("RegisterCtrlCom", RegisterCtrlCom)

    function RegisterCtrlCom($state, docService) {
        var vm = this;

        vm.doc = docService.getNewDoc();

        vm.submit = function () {
            console.log("Saving account setup details");
            docService.saveUser(vm.doc)
                .then(function () {
                    $state.go("register-doc2_social");
                })
                .catch(function(){
                    alert("Try Again")
                })
        }
    }

    RegisterCtrlCom.$inject = ["$state","docService"]
})();



