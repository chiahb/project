/**
 * Created by Francis Chia on 8/15/2016.
 */

(function () {
    angular.module("dowApp")
        .controller("RegisterCtrl", RegisterCtrl)

    function RegisterCtrl($state, docService) {
        var vm = this;

        //check current user
            vm.doc = docService.getNewDoc();

            vm.submit = function () {
                console.log("Saving doctor credentials");
                // docService.saveDoc(vm.doc)
                //     .then(function () {
                        $state.go("register-doc4-confirm");
                    // })
                    // .catch(function(){
                    //     alert("Try Again")
                    // })
        }
    }
    RegisterCtrl.$inject = ["$state", "docService"]
})();

