/**
 * Created by Francis Chia on 8/1/2016.
 */

(function () {
    angular
        .module("dowApp")
        .config(HealthConfig);

    function HealthConfig($stateProvider, $urlRouterProvider){
            $stateProvider
                .state("home", {
                    url: "/",
                    templateUrl: "/views/home.html",
                })
                .state("register", {
                    url: "/register",
                    templateUrl: "/views/reg.html",
                })
                .state("register-common", {
                    url: "/register/:userType",
                    templateUrl: "/views/reg_com1_acct_setup.html",
                    controller: "RegisterCtrlCom as ctrl"
                })
                .state("register-doc2_social", {
                    url: "/register/doctor/form1",
                    templateUrl: "/views/reg_doc2_social.html",
                    controller: "RegisterCtrlDoc2 as ctrl"
                })

                .state("register-doc3_credentials", {
                    url: "/register/doctor/form2",
                    templateUrl: "/views/reg_doc3_credentials.html",
                    controller: "RegisterCtrl as ctrl"
                })
                
                .state("register-doc4-confirm", {
                    url: "/register/doctor/confirm",
                    templateUrl: "/views/reg_doc4_cfm.html",
                    // controller: function ($stateParams, $state) {
                    //     var vm = this;
                    //     //check current user
                    //     vm.submit = function () {
                    //         // console.log("Save to DB later");
                    //         // $state.go("register-confirmation")
                    //     }
                    // },
                    // controllerAs : "ctrl"
                })
                
                .state("register-patient", {
                    url: "/register/patient/form",
                    templateUrl: "/views/reg_pat2.html",
                })

                // .state("register-patient", {
                //     url: "/register/patient/form/social",
                //     templateUrl: "/views/reg-pat2.html",
                // })
                //

                .state("login", {
                    url: "/login",
                    templateUrl: "/views/login.html",
                });

        // .state("parent", {
        //     url: "/parent",
        //     template: "<h1>I'm Parent</h1><div ui-view></div>"
        // })
        // .state("parent.child1", {
        //     url: "/child1",
        //     template:"<div>I'm child 1</div>"
        // })
        // .state("parent.child2", {
        //     url: "/child2",
        //     template:"<div>I'm child 2</div>"
        // })
            $urlRouterProvider.otherwise("/");
        };

    HealthConfig.$inject = ["$stateProvider", "$urlRouterProvider"];
})();