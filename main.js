
/**
 * Created by Francis Chia on 8/1/2016.
 */

//Load express and other
var express = require("express");
var bodyParser = require("body-parser");
var routes = require("./routes");
var validator = require('express-validator')

//Create an instance of express application
var app = express();

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(validator());

app.use(express.static(__dirname + "/public"));
app.use("/bower_components", express.static(__dirname + "/bower_components"));


app.post("/api/doc/saveUser", routes.saveUser); //Create a Doctor's record
app.post("/api/doc/saveDoc", routes.saveDoc); //Create a Doctor's record
app.get("/api/doc/list", routes.listDoc); //Read or List Doctor records
app.put("/api/doc/update", routes.updateDoc); //Update a Doctor record
app.delete("/api/doc/delete", routes.deleteDoc);  //Delete a Doctor record


// app.post("/api/patlist/save", routes.savePat); //Create a Patient record
// app.get("/api/patlist/list", routes.listPat; //Read or List Patient records
// app.put("/api/patlist/update", routes.updatePat); //Update a Patient record
// app.delete("/api/patlist/delete", routes.deletePat);  //Delete a Patient record


// var api_key = 'key-60e9246ca4884998ae1838294dbd434c';
// var domain = 'sandboxe85efaa946964e5f875d6c179d709b3c.mailgun.org';
// var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});
//
// var data = {
//     from: 'Excited User <postmaster@sandboxe85efaa946964e5f875d6c179d709b3c.mailgun.org>',
//     to: 'chiahb@yahoo.com',
//     subject: 'Hello Awesome',
//     text: 'Testing some Mailgun awesomness!'
// };
//
// mailgun.messages().send(data, function (error, body) {
//     console.log(body);
// });

app.listen(3000, function () {
    console.log("Listening on ", 3000)
});












